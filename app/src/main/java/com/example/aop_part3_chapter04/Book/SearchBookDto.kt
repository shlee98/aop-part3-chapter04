package com.example.aop_part3_chapter04.Book

import com.google.gson.annotations.SerializedName

data class SearchBookDto (
    @SerializedName("items") val books: List<Book>

)