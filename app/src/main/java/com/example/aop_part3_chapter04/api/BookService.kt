package com.example.aop_part3_chapter04.api

import com.example.aop_part3_chapter04.Book.SearchBookDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface BookService {

    @Headers(
        "X-Naver-Client-Id: TGdk54LHFUpciNfyRu3Z",
        "X-Naver-Client-Secret: b9dnBBEOaJ"
    )
    @GET("/v1/search/book.json")
    fun getBooksByName (
        @Query("query") keyword: String
    ): Call<SearchBookDto>

}