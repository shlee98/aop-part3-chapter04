package com.example.aop_part3_chapter04

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.example.aop_part3_chapter04.Book.History
import com.example.aop_part3_chapter04.Book.SearchBookDto
import com.example.aop_part3_chapter04.adapter.BookAdapter
import com.example.aop_part3_chapter04.adapter.HistoryAdapter
import com.example.aop_part3_chapter04.api.BookService
import com.example.aop_part3_chapter04.databinding.ActivityMainBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: BookAdapter
    private lateinit var historyAdapter: HistoryAdapter
    private lateinit var bookService: BookService
    private lateinit var db: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initBookRecyclerView()
        initHistoryRecyclerView()
        initSearchEditText()

        db = getAppDatabase(this)

        val retrofit = Retrofit.Builder()
            .baseUrl("https://openapi.naver.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        bookService = retrofit.create(BookService::class.java)

        bookService.getBooksByName(
            ANDROID
        ).enqueue(object : Callback<SearchBookDto>{
                override fun onResponse(
                    call: Call<SearchBookDto>,
                    response: Response<SearchBookDto>
                ) {
                    //TODO 성공 처리
                    if (response.isSuccessful.not()) {
                        Log.e(TAG, "Not Success!")
                        return
                    }

                    response.body()?.let {
                        adapter.submitList(it.books)
                    }

                }

                override fun onFailure(call: Call<SearchBookDto>, t: Throwable) {
                    // TODO 실패 처리
                    Log.e(TAG, t.toString())
                }

            })


    }

    private fun search(keyword: String) {
        bookService.getBooksByName(
            keyword
        ) .enqueue(object : Callback<SearchBookDto>{
            override fun onResponse(
                call: Call<SearchBookDto>,
                response: Response<SearchBookDto>
            ) {
                //TODO 성공 처리

                hideHistoryView()
                saveSearchKeyword(keyword)

                if (response.isSuccessful.not()) {
                    Log.e(TAG, "Not Success!")
                    return
                }

                response.body()?.let {
                    adapter.submitList(it.books)
                }

            }

            override fun onFailure(call: Call<SearchBookDto>, t: Throwable) {
                // TODO 실패 처리
                hideHistoryView()
                Log.e(TAG, t.toString())
            }

        })
    }

    private fun showHistoryView() {
        Thread{
            val keywords = db.historyDao().getAll().reversed()

            runOnUiThread {
                binding.historyRecyclerView.isVisible = true
                historyAdapter.submitList(keywords.orEmpty())
            }

        }.start()

        binding.historyRecyclerView.isVisible = true
    }

    private fun hideHistoryView() {
        binding.historyRecyclerView.isVisible = false
    }

    private fun saveSearchKeyword(keyword: String){
        Thread{
            db.historyDao().delete(keyword)
            db.historyDao().insertHistory(History(null, keyword))
        }.start()
    }

    private fun initBookRecyclerView() {
        adapter = BookAdapter(itemClickListener = {
            val intent = Intent(this, DetailActivity::class.java)
            intent.putExtra("bookModel", it)
            startActivity(intent)
        })

        binding.bookRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.bookRecyclerView.adapter = adapter
    }

    private fun initHistoryRecyclerView() {
        historyAdapter = HistoryAdapter(historyDeleteClickListener = {
            deleteSearchKeyword(it)
        }, historyClickListener = {
            searchKeyword(it)
        })

        binding.historyRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.historyRecyclerView.adapter = historyAdapter
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initSearchEditText() {
        binding.searchEditText.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == MotionEvent.ACTION_DOWN) {
                search(binding.searchEditText.text.toString())
                return@setOnKeyListener true
            }

            return@setOnKeyListener false
        }

        binding.searchEditText.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                showHistoryView()
            }
            return@setOnTouchListener false
        }

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && binding.historyRecyclerView.isVisible) {
            // 뒤로가기 버튼을 눌렀을 때 수행할 동작을 여기에 작성합니다.
            hideHistoryView()
            return true // 이벤트를 소비했음을 반환
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun deleteSearchKeyword(keyword: String){
        Thread{
            db.historyDao().delete(keyword)
            showHistoryView()
        }.start()
    }

    private fun searchKeyword(keyword: String) {
        search(keyword)
        binding.searchEditText.setText(keyword)
    }

    companion object {
        private const val TAG = "MainActivity"
        private const val ANDROID = "android"
    }


}